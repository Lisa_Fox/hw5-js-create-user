// ТЕОРЕТИЧНІ ПИТАННЯ
// 1. Об`єкт зберігає в собі парну структуру: це властивісь(ключ) та значення. 
//НАПРИКЛАД: якщо значенням властивості являється функція, то ця функція - це вже метод об`єкту. 
// Тобто це те, що дозволяє працювати із значеннями властивостей та методами всередині заданого обʼєкта.
// 2. Будь-який, наприклад: строка, число, null, undefind, булеве значення, ще один обєкт і т.д..
// 3. Обʼєкт - займає визначену ячейку памʼяті. 
//І ми можемо присвоїти посилання та значення обʼєкта - новому створеному, 
//і це буде лежати в одній і тій самій ячейці памʼяті.
//Наприклад, ми можемо мати папку з файлом на жорсткому диску, на робочому столі створити ярлик 
//з іншою назвою, але це буде один і той самий файл. 

//ПРАКТИЧНЕ ЗАВДАННЯ

function createNewUser() {
    let firstName = prompt("Enter your name");
    let lastName = prompt("Enter your last name");
  return {
    _firstName: firstName,
    _lastName: lastName,
    getLogin() {
      return (this._firstName[0] + this._lastName).toLowerCase();
    },
    setFirstName(value) {
      this._firstName = value;
    },
    setLastName(value) {
      this._lastName = value;
    },
// Альтернативний спосіб set "з коробки"
    // set firstName(value) {
    //   this._firstName = value;
    // },
    // set lastName(value) {
    //   this._lastName = value;
    // },
  };
}
let newUser = createNewUser();
console.log(newUser.getLogin());

